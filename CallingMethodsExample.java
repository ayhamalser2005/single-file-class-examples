public class CallingMethodsExample{

    public static void main(String []args){
        String shortSentence = "Hi!";
        String longSentence = "Hi, how are you today? I am well!";
        
        shortSentence.length();
        System.out.println(longSentence.length());
        
        
        String capSentence = longSentence.toUpperCase();
        System.out.println(capSentence);
    }
}


