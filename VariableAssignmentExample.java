public class VariableAssignmentExample{

    public static void main(String []args){
        //start by declaring and intializing the variable
        int theVariable = 2;
        System.out.println(theVariable);
        
        //change the variable
        theVariable = 3;
        System.out.println(theVariable);
        
        //add to the variable
        theVariable = theVariable + 5;
        System.out.println(theVariable);
    }
}
