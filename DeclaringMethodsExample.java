public class DeclaringMethodsExample{

    public static void main(String []args){
        printArea(3,5);
        System.out.println(calculateArea(2,3));
        printArea();
    }
     
    public static void printArea(int length, int width){
        int area;
        area = length * width; 
        System.out.println(area);
    }
     
    public static int calculateArea(int length, int width){
        int area;
        area = length * width;
        return area;
    }
	 
    public static void printArea(){
        System.out.println("Area");
    }
}


